package Database;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class DataBase {

    private Connection connection;

    public double getCost(String name, String value){
        if(connection == null)
            createConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet;
            if(name.equals("calibre") || name.equals("patron"))
                resultSet = statement.executeQuery("SELECT cost FROM " + name +
                        " WHERE value = (\'" + value.trim()+ "\':: varchar )");
            else
                resultSet = statement.executeQuery("SELECT cost FROM " + name +
                        " WHERE min <= " + value + " AND " + value + " <= max" );
            double cost = -1;
            if(resultSet.next())
                cost = resultSet.getDouble("cost");
            statement.close();
            resultSet.close();
            connection.close();
            return cost;
        }
        catch (SQLException e){
            e.getErrorCode();
        }
        return -1;
    }

    public ArrayList<String> getValue(String name){
        if(connection == null)
            createConnection();
        try {
            ArrayList<String> strings = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT value FROM " + name );
            while(resultSet.next())
               strings.add(resultSet.getString("value"));
            statement.close();
            resultSet.close();
            connection.close();
            return strings;
        }
        catch (SQLException e){
            e.getErrorCode();
        }
        return null;
    }

    public int getMax(String name){
        if(connection == null)
            createConnection();
        try {
            int i = 0;
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT max FROM " + name );
            while(resultSet.next())
                i = (resultSet.getInt("max"));
            statement.close();
            resultSet.close();
            connection.close();
            return i;
        }
        catch (SQLException e){
            e.getErrorCode();
        }
        return 0;
    }

    public int getMin(String name){
        if(connection == null)
            createConnection();
        try {
            int i = 0;
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT min FROM " + name );
            if(resultSet.next())
                i = resultSet.getInt("min");
            statement.close();
            resultSet.close();
            connection.close();
            return i;
        }
        catch (SQLException e){
            e.getErrorCode();
        }
        return 0;
    }

    private void createConnection(){
        String[] strings = new String[4];
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("start.txt"));
            bufferedReader.readLine();
            for(int i = 0; i < 4; i++) {
                strings[i] = bufferedReader.readLine();
                while (strings[i].contains("  "))
                    strings[i] = strings[i].replace("  "," ");
            }
        }
        catch (IOException e){
            e.getMessage();
        }
        String username = strings[0].split(" ")[1].trim();
        String password = strings[1].split(" ")[1].trim();
        String URL =strings[2].split(" ")[1].trim();
        try{
            Class.forName(strings[3].split(" ")[1].trim());
            connection =  DriverManager.getConnection(URL,username,password);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
