package Database;

import com.opencsv.CSVReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class DataCSV {

    private String filepath;

    public double getCost(String name, String value){
        try {
            CSVReader csvReader = new CSVReader(new FileReader("options_csv\\" + name +".csv"));
            double cost = 0;
            String[] strings;
            while((strings = csvReader.readNext()) != null){
                for(int i = 0; i < strings.length; i++) {
                    if (name.equals("calibre") || name.equals("patron")){
                        if (strings[i].trim().equals(value.trim())){
                                cost = Double.valueOf(strings[2]);
                                break;
                            }
                    }
                    else if(Integer.parseInt(strings[2].trim()) <= Double.valueOf(value)
                    && Integer.parseInt(strings[3].trim()) > Double.valueOf(value) ) {
                        cost = Double.valueOf(strings[1]);
                        break;
                    }
                }
            }
            csvReader.close();
            return cost;
        }
        catch (Exception e){
            e.getMessage();
        }
        return 0;
    }


    public double getMax(String name){
        try {
            CSVReader csvReader = new CSVReader(new FileReader("options_csv\\" + name + ".csv"));
            List<String[]> list = csvReader.readAll();
            String s = list.get(list.size()-1)[list.get(list.size()-1).length-1];
            csvReader.close();
            return Double.valueOf(s);
        }
        catch (Exception e){
            e.getMessage();
        }
        return 0;
    }

    public double getMin(String name){
        try {
            CSVReader csvReader = new CSVReader(new FileReader("options_csv\\" + name + ".csv"));
            String s = csvReader.readNext()[2];
            csvReader.close();
            return Double.valueOf(s);
        }
        catch (Exception e){
            e.getMessage();
        }
        return 0;
    }

    public ArrayList<String> getValue(String name){
        try {
            CSVReader csvReader = new CSVReader(new FileReader("options_csv\\" + name + ".csv"));
            List<String[]> strings = csvReader.readAll();
            ArrayList<String> s = new ArrayList<>();
            for(String[] i : strings)
                s.add(i[1]);
            csvReader.close();
            return s;
        }
        catch (Exception e){
            e.getMessage();
        }
        return null;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

}
