package Weapon;

import Database.*;

public class BladeWeapon extends AbstractWeapon {

    private double lenght;

    public BladeWeapon(){    }
    public BladeWeapon(String name, double weight, double lenght){
        super(name,weight);
        setLenght(lenght);
    }

    @Override
    public void setCost() {
        this.cost = 0;
        this.cost += new DataCSV().getCost("lenght", Double.toString(lenght)) + (int)getWeight()*100;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public double getLenght() {
        return lenght;
    }
}
