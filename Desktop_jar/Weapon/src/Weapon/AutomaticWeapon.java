package Weapon;

import Database.*;

public class AutomaticWeapon extends FireArm{

    private double rate;

    public AutomaticWeapon(){}
    public AutomaticWeapon(String name, double weight, double distance, String caliber, double speed, int capacity,double rate){
        super(name,weight,distance,caliber,speed,capacity);
        setRate(rate);
    }

    @Override
    public void setCost() {
        super.setCost();
        cost += new DataCSV().getCost("rate",Double.toString(rate));
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
