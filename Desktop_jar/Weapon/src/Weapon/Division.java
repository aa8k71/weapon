package Weapon;

import java.util.HashMap;
import java.util.Map;

public class Division {

    private Map<AbstractWeapon,Integer> weapons = new HashMap<AbstractWeapon,Integer>();

    public Map<AbstractWeapon, Integer> getWeapons() {
        return weapons;
    }

    public void setWeapons(Map<AbstractWeapon, Integer> weapons) {
        this.weapons = weapons;
    }

    public void addWeapon(AbstractWeapon weapon, int volume){
        if(!weapons.containsKey(weapon))
            weapons.putIfAbsent(weapon,volume);
        else weapons.put(weapon,weapons.get(weapon)+volume);
    }

    public void addWeapon(AbstractWeapon weapon){
        addWeapon(weapon,1);
    }

    public void removeWeapon(AbstractWeapon weapon, int volume){
        if(!weapons.containsKey(weapon))
            return;
        if(weapons.get(weapon) <= volume)
            weapons.remove(weapon);
        else weapons.put(weapon,weapons.get(weapon)-volume);
    }

    public void removeWeapon(AbstractWeapon weapon){
        removeWeapon(weapon,1);
    }

    public double countCost(){
        double cost = 0;
        for(Map.Entry<AbstractWeapon,Integer> i : weapons.entrySet())
            cost += i.getKey().getCost()*i.getValue();
        return cost;
    }
}
