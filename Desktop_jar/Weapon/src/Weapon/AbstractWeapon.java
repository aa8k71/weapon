package Weapon;

public abstract class AbstractWeapon {

    private String name;
    private double weight;
    protected double cost;

    public AbstractWeapon(){}
    public AbstractWeapon(String name, double weight){
        setName(name);
        setWeight(weight);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public abstract void setCost();

    public double getCost() {
        if(cost == 0)
            setCost();
        return cost;
    }

}
