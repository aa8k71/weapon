package Weapon;

import Database.*;

public class SemiAutomaticWeapon extends FireArm{

    private String patron;

    public SemiAutomaticWeapon(){}
    public SemiAutomaticWeapon(String name, double weight, double distance, String caliber, double speed, int capacity,String patron){
        super.setCaliber(caliber);
        super.setCapacity(capacity);
        super.setDistance(distance);
        super.setName(name);
        super.setWeight(weight);
        super.setSpeed(speed);
        setPatron(patron);
    }

    public void setCost(){
        super.setCost();
        cost += new DataCSV().getCost("patron", patron)*getCapacity();
    }

    public String getPatron() {
        return patron;
    }

    public void setPatron(String patron) {
        this.patron = patron;
    }
}
