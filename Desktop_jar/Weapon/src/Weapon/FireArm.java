package Weapon;

import Database.*;

public class FireArm extends AbstractWeapon {

    private double distance;
    private String caliber;
    private double speed;
    private int capacity;

    public FireArm(){}
    public FireArm(String name, double weight, double distance, String caliber, double speed, int capacity){
        super(name,weight);
        setDistance(distance);
        setCapacity(capacity);
        setCaliber(caliber);
        setSpeed(speed);
    }

    @Override
    public void setCost() {
        this.cost = 0;
        this.cost += capacity * new DataCSV().getCost("calibre",caliber);
        this.cost += new DataCSV().getCost("distance", Double.toString(distance));
        this.cost += new DataCSV().getCost("speed", Double.toString(speed));
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public String getCaliber() {
        return caliber;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getSpeed() {
        return speed;
    }

}
