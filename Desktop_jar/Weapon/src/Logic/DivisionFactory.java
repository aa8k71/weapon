package Logic;

import Weapon.*;

public class DivisionFactory {

    public static Division createDivision(){
        Division division = new Division();
        AutomaticWeapon m_4 = new AutomaticWeapon("M4",2.9,500,"5.56x.45",936,30,700);
        AutomaticWeapon ak_103 = new AutomaticWeapon("Ak-103", 4.1, 1000,"7.62x.56",910,30,600);
        SemiAutomaticWeapon ruger_mini_14  = new SemiAutomaticWeapon("Ruger Mini-14 ", 2.9, 550,"6.8",945,5,"Remington_SCP");
        FireArm svd = new FireArm("СВД", 4.3,1300,"12.7",960,10);
        BladeWeapon machete = new BladeWeapon("Мачете", 1.2,50);

        division.addWeapon(m_4);
        division.addWeapon(ak_103,5);
        division.addWeapon(ruger_mini_14);
        division.addWeapon(svd);
        division.addWeapon(machete);

        return division;
    }
}
