package DivisionConsole;

import Weapon.AbstractWeapon;
import Weapon.Division;
import Logic.*;

import java.util.Map;

public class DivisionPrinter {

    public static void print(Division division){
        System.out.println("Перечень оружия\n" + "Название\t\tКоличество\tСтоимость");
        for(Map.Entry<AbstractWeapon,Integer> i : division.getWeapons().entrySet())
            System.out.format("%-16s%-12s%s  \n", i.getKey().getName(), i.getValue(),i.getKey().getCost());
        System.out.print("\nОбщая стоимость: " + division.countCost());
    }

}
