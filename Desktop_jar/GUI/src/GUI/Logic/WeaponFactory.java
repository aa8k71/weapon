package GUI.Logic;

import Weapon.AutomaticWeapon;
import Weapon.BladeWeapon;
import Weapon.FireArm;
import Weapon.SemiAutomaticWeapon;

import java.util.HashMap;

public class WeaponFactory {

    public static FireArm createFireArm(HashMap<String,String> hashMap){
        FireArm fireArm = new FireArm();
        editFireArm(hashMap, fireArm);
        return fireArm;
    }

    public static void editFireArm(HashMap<String,String> hashMap, FireArm fireArm){
        fireArm.setName(hashMap.get("name"));
        fireArm.setDistance(Double.valueOf(hashMap.get("distance")));
        fireArm.setSpeed(Double.valueOf(hashMap.get("speed")));
        fireArm.setCapacity(Integer.valueOf(hashMap.get("capacity")));
        fireArm.setCaliber(hashMap.get("calibre"));
        fireArm.setCost();
    }

    public static AutomaticWeapon createAutomaticWeapon(HashMap<String,String> hashMap){
        AutomaticWeapon automaticWeapon = new AutomaticWeapon();
        editAutomaticWeapon(hashMap, automaticWeapon);
        return automaticWeapon;
    }

    public static void editAutomaticWeapon(HashMap<String,String> hashMap, AutomaticWeapon automaticWeapon){
        automaticWeapon.setName(hashMap.get("name"));
        automaticWeapon.setDistance(Double.valueOf(hashMap.get("distance")));
        automaticWeapon.setSpeed(Double.valueOf(hashMap.get("speed")));
        automaticWeapon.setCapacity(Integer.valueOf(hashMap.get("capacity")));
        automaticWeapon.setCaliber(hashMap.get("calibre"));
        automaticWeapon.setRate(Double.valueOf(hashMap.get("rate")));
        automaticWeapon.setCost();
    }

    public static SemiAutomaticWeapon createSemiAutomaticWeapon(HashMap<String,String> hashMap){
        SemiAutomaticWeapon semiAutomaticWeapon = new SemiAutomaticWeapon();
        editSemiAutomaticWeapon(hashMap, semiAutomaticWeapon);
        return semiAutomaticWeapon;
    }

    public static void editSemiAutomaticWeapon(HashMap<String,String> hashMap, SemiAutomaticWeapon semiAutomaticWeapon){
        semiAutomaticWeapon.setName(hashMap.get("name"));
        semiAutomaticWeapon.setDistance(Double.valueOf(hashMap.get("distance")));
        semiAutomaticWeapon.setSpeed(Double.valueOf(hashMap.get("speed")));
        semiAutomaticWeapon.setCapacity(Integer.valueOf(hashMap.get("capacity")));
        semiAutomaticWeapon.setCaliber(hashMap.get("calibre"));
        semiAutomaticWeapon.setPatron(hashMap.get("patron"));
        semiAutomaticWeapon.setCost();
    }

    public static BladeWeapon createBladeWeapon(HashMap<String,String> hashMap){
        BladeWeapon bladeWeapon = new BladeWeapon();
        editBladeWeapon(hashMap,bladeWeapon);
        return bladeWeapon;
    }

    public static void editBladeWeapon(HashMap<String,String> hashMap, BladeWeapon bladeWeapon){
        bladeWeapon.setName(hashMap.get("name"));
        bladeWeapon.setLenght(Double.valueOf(hashMap.get("lenght")));
        bladeWeapon.setCost();
    }
}
