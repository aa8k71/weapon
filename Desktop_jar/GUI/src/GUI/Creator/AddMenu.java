package GUI.Creator;

import GUI.Creator.JPanelWeapon.*;
import GUI.DivisionDesktop.MainMenu;
import GUI.Logic.WeaponFactory;
import GUI.Validator.Handling;
import Weapon.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AddMenu extends JFrame {

    private JPanelWeapon jPanel;
    private JButton jButton_cancel;
    private JButton jButton_ok;
    private JComboBox jComboBox_class;
    private JLabel jLabel_class = new JLabel("Class:");
    private DefaultComboBoxModel<String> defaultComboBoxModel_class;
    private AbstractWeapon abstractWeapon = null;

    public AddMenu(){}
    public AddMenu(int width, int height, MainMenu mainMenu){
        setTitle("AddWeapon");
        setBounds(Toolkit.getDefaultToolkit().getScreenSize().width/2-width/2,
                Toolkit.getDefaultToolkit().getScreenSize().height/2 - height/2, width,height);
        jPanel = new JPanelFireArm();
        setjComboBox_class();
        setjButton_cancel(mainMenu);
        setjButton_ok(mainMenu);
        setjPanel();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                mainMenu.setEnabled(true);
                setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            }
        });
        setResizable(false);
    }
    public AddMenu(int width,int height, MainMenu mainMenu, AbstractWeapon abstractWeapon){
        setTitle("EditWeapon");
        setBounds(Toolkit.getDefaultToolkit().getScreenSize().width/2-width/2,
                Toolkit.getDefaultToolkit().getScreenSize().height/2 - height/2, width,height);
        setAbstractWeapon(abstractWeapon);
        setjComboBox_class();
        setjButton_cancel(mainMenu);
        setjButton_ok(mainMenu);
        setjPanel(abstractWeapon);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                mainMenu.setEnabled(true);
                setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            }
        });
        setResizable(false);
    }

    private void setjPanel(){
        jPanel.setLayout(null);
        jLabel_class.setBounds(20,5,300,20);
        jPanel.add(jButton_cancel);
        jPanel.add(jButton_ok);
        jPanel.add(jComboBox_class);
        setContentPane(jPanel);
        setVisible(true);
    }

    private void setjPanel(AbstractWeapon abstractWeapon){
        if(abstractWeapon instanceof SemiAutomaticWeapon) {
            jComboBox_class.setSelectedIndex(2);
            jComboBox_class.setEnabled(false);
            jPanel.setText(abstractWeapon);
            return;
        }
        if(abstractWeapon instanceof AutomaticWeapon){
            jComboBox_class.setSelectedIndex(1);
            jComboBox_class.setEnabled(false);
            jPanel.setText(abstractWeapon);
            return;
        }
        if(abstractWeapon instanceof BladeWeapon) {
            jComboBox_class.setSelectedIndex(3);
            jComboBox_class.setEnabled(false);
            jPanel.setText(abstractWeapon);
            return;
        }
        if(abstractWeapon instanceof FireArm) {
            jComboBox_class.setSelectedIndex(0);
            jPanel.setText(abstractWeapon);
        }
        jComboBox_class.setEnabled(false);
    }

    private void setjButton_cancel(MainMenu mainMenu){
        jButton_cancel = new JButton("cancel");
        jButton_cancel.setBounds(175,350,100,40);
        jButton_cancel.addActionListener( (ActionEvent e) -> {
                mainMenu.setEnabled(true);
                dispose();
            }
        );
    }

    private void setjButton_ok(MainMenu mainMenu){
        jButton_ok = new JButton("ok");
        jButton_ok.setBounds(65,350,100,40);
        jButton_ok.addActionListener((ActionEvent e) ->{
            jPanel.setColor();
            if(!Handling.handlingNull(jPanel))
                return;
            if(!Handling.handlingWrongNumber(jPanel))
                return;
            if(!Handling.handlingWrongValue(jPanel))
                return;
            switch (jComboBox_class.getSelectedIndex()){
                case 0:
                    if(abstractWeapon == null) {
                        abstractWeapon = WeaponFactory.createFireArm(jPanel.getText());
                        mainMenu.addNewWeapon(abstractWeapon);
                    }
                    else {
                        WeaponFactory.editFireArm(jPanel.getText(), (FireArm) this.abstractWeapon);
                        mainMenu.updateJList();
                    }
                    break;
                case 1:
                    if(abstractWeapon == null) {
                        abstractWeapon = WeaponFactory.createAutomaticWeapon(jPanel.getText());
                        mainMenu.addNewWeapon(abstractWeapon);
                    }
                    else {
                        WeaponFactory.editAutomaticWeapon(jPanel.getText(), (AutomaticWeapon) this.abstractWeapon);
                        mainMenu.updateJList();
                    }
                    break;
                case 2:
                    if(abstractWeapon == null) {
                        abstractWeapon = WeaponFactory.createSemiAutomaticWeapon(jPanel.getText());
                        mainMenu.addNewWeapon(abstractWeapon);
                    }
                    else {
                        WeaponFactory.editSemiAutomaticWeapon(jPanel.getText(), (SemiAutomaticWeapon) this.abstractWeapon);
                        mainMenu.updateJList();
                    }
                    break;
                case 3:
                    if(abstractWeapon == null) {
                        abstractWeapon = WeaponFactory.createBladeWeapon(jPanel.getText());
                        mainMenu.addNewWeapon(abstractWeapon);
                    }
                    else {
                        WeaponFactory.editBladeWeapon(jPanel.getText(), (BladeWeapon) this.abstractWeapon);
                        mainMenu.updateJList();
                    }
                    break;
            }
            mainMenu.setEnabled(true);
            dispose();
        });
    }

//    private void setjButton_ok(MainMenu mainMenu, AbstractWeapon abstractWeapon){
//        jButton_ok = new JButton("ok");
//        jButton_ok.setBounds(65,350,100,40);
//        setAbstractWeapon(abstractWeapon);
//        jButton_ok.addActionListener((ActionEvent e) ->{
//            jPanel.setColor();
//            if(!Handling.handlingNull(jPanel))
//                return;
//            if(!Handling.handlingWrongNumber(jPanel))
//                return;
//            if(!Handling.handlingWrongValue(jPanel))
//                return;
//            switch (jComboBox_class.getSelectedIndex()){
//                case 0:
//                    WeaponFactory.editFireArm(jPanel.getText(),(FireArm)this.abstractWeapon);
//                    break;
//                case 1:
//                    WeaponFactory.editAutomaticWeapon(jPanel.getText(),(AutomaticWeapon)this.abstractWeapon);
//                    break;
//                case 2:
//                    WeaponFactory.editSemiAutomaticWeapon(jPanel.getText(),(SemiAutomaticWeapon)this.abstractWeapon);
//                    break;
//                case 3:
//                    WeaponFactory.editBladeWeapon(jPanel.getText(),(BladeWeapon)this.abstractWeapon);
//                    break;
//            }
//            mainMenu.updateJList();
//            mainMenu.setEnabled(true);
//            dispose();
//        });
//    }

    private void setjComboBox_class(){
        jComboBox_class = new JComboBox();
        setDefaultComboBoxModel_class();
        jComboBox_class.setBounds(20,25, 300,40);
        jComboBox_class.addActionListener((ActionEvent e) ->{
            int i = jComboBox_class.getSelectedIndex();
            switch (i){
                case 0:
                    jPanel = new JPanelFireArm();
                    break;
                case 1:
                    jPanel = new JPanelAutomaticWeapon();
                    break;
                case 2:
                    jPanel = new JPanelSemiAutomaticWeapon();
                    break;
                case 3:
                    jPanel = new JPanelBladeWeapon();
                    break;
            }
            setjPanel();
        });
    }

    private void setDefaultComboBoxModel_class(){
        String[] strings = {"Что то стреляющее","Автоматическое оружие",
                "Полуавтоматическое оружие","Холодное оружие"};
        defaultComboBoxModel_class = new DefaultComboBoxModel<>(strings);
        jComboBox_class.setModel(defaultComboBoxModel_class);
    }

    private void setAbstractWeapon(AbstractWeapon abstractWeapon){
        this.abstractWeapon = abstractWeapon;
    }
}
