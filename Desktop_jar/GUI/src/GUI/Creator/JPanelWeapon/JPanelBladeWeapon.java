package GUI.Creator.JPanelWeapon;

import Weapon.AbstractWeapon;
import Weapon.BladeWeapon;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class JPanelBladeWeapon extends JPanelWeapon {

    private JTextField jTextField_lenght = new JTextField();
    private JLabel jLabel_lenght = new JLabel("Length:");

    public JPanelBladeWeapon(){
        setjLabel(jLabel_lenght,115);
        setjTextField(jTextField_lenght,135);
    }

    @Override
    public HashMap<String,String> getText() {
        HashMap<String,String> strings = new HashMap<>(super.getText());
        strings.put("lenght",jTextField_lenght.getText());
        return strings;
    }

    @Override
    public void paint(ArrayList<String> nameLabel) {
        super.paint(nameLabel);
        if(nameLabel.contains("lenght"))
            jLabel_lenght.setForeground(Color.red);
    }

    @Override
    public void setColor() {
        super.setColor();
        jLabel_lenght.setForeground(Color.BLACK);
    }

    @Override
    public void setText(AbstractWeapon abstractWeapon) {
        jTextField_lenght.setText(String.valueOf((int)((BladeWeapon)abstractWeapon).getLenght()));
        super.setText(abstractWeapon);
    }
}
