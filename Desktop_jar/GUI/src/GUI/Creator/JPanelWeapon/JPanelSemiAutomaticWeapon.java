package GUI.Creator.JPanelWeapon;

import Weapon.AbstractWeapon;
import Weapon.SemiAutomaticWeapon;

import javax.swing.*;
import java.util.HashMap;

public class JPanelSemiAutomaticWeapon extends JPanelFireArm {

    private JComboBox jComboBox_patron = new JComboBox();
    private JLabel jLabel_patron = new JLabel("Patron:");

    public JPanelSemiAutomaticWeapon(){
        setjComboBox(jComboBox_patron,315,"patron");
        setjLabel(jLabel_patron,295);
    }

    @Override
    public HashMap<String,String> getText() {
        HashMap<String,String> strings = new HashMap<>(super.getText());
        strings.put("patron",(String)jComboBox_patron.getSelectedItem());
        return strings;
    }

    @Override
    public void setText(AbstractWeapon abstractWeapon) {
        jComboBox_patron.setSelectedItem(((SemiAutomaticWeapon)abstractWeapon).getPatron());
        super.setText(abstractWeapon);
    }
}
