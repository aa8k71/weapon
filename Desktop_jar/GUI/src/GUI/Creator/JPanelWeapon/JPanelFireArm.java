package GUI.Creator.JPanelWeapon;

import Database.DataCSV;
import Weapon.AbstractWeapon;
import Weapon.FireArm;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class JPanelFireArm extends JPanelWeapon {

    private JComboBox jComboBox_calibre = new JComboBox();
    private JTextField jTextField_distance = new JTextField();
    private JTextField jTextField_speed = new JTextField();
    private JTextField jTextField_capacity = new JTextField();
    private JLabel jLabel_distance = new JLabel("Distance:");
    private JLabel jLabel_capacity = new JLabel("Capacity:");
    private JLabel jLabel_caliber = new JLabel("Calibre:");
    private JLabel jLabel_speed = new JLabel("Speed:");

    public JPanelFireArm(){
        setjComboBox(jComboBox_calibre,135,"calibre");
        setjLabel(jLabel_caliber,115);
        setjTextField(jTextField_distance,180);
        setjLabel(jLabel_distance,160);
        setjTextField(jTextField_speed,225);
        setjLabel(jLabel_speed,205);
        setjTextField(jTextField_capacity,270);
        setjLabel(jLabel_capacity,250);
    }

    void setjComboBox(JComboBox jComboBox, int y, String name){
        DefaultComboBoxModel<String> defaultComboBoxModel = new DefaultComboBoxModel<>();
        ArrayList<String> stringArrayList = new DataCSV().getValue(name);
        for(String i : stringArrayList)
            defaultComboBoxModel.addElement(i.trim());
        jComboBox.setBounds(20,y,300,20);
        jComboBox.setModel(defaultComboBoxModel);
        add(jComboBox);
    }

    @Override
    public HashMap<String,String> getText(){
        HashMap<String,String> strings = new HashMap<>(super.getText());
        strings.put("calibre",(String)jComboBox_calibre.getSelectedItem());
        strings.put("distance",jTextField_distance.getText());
        strings.put("speed",jTextField_speed.getText());
        strings.put("capacity",jTextField_capacity.getText());
        return strings;
    }

    @Override
    public void paint(ArrayList<String> nameLabel){
        super.paint(nameLabel);
        if(nameLabel.contains("distance"))
            jLabel_distance.setForeground(Color.red);
        if(nameLabel.contains("speed"))
            jLabel_speed.setForeground(Color.red);
        if(nameLabel.contains("capacity"))
            jLabel_capacity.setForeground(Color.red);
    }

    @Override
    public void setColor() {
        super.setColor();
        jLabel_distance.setForeground(Color.BLACK);
        jLabel_speed.setForeground(Color.BLACK);
        jLabel_capacity.setForeground(Color.BLACK);
    }

    @Override
    public void setText(AbstractWeapon abstractWeapon) {
        jComboBox_calibre.setSelectedItem(((FireArm)abstractWeapon).getCaliber());
        jTextField_capacity.setText(String.valueOf(((FireArm)abstractWeapon).getCapacity()));
        jTextField_distance.setText(String.valueOf((int)((FireArm)abstractWeapon).getDistance()));
        jTextField_speed.setText(String.valueOf((int)((FireArm)abstractWeapon).getSpeed()));
        super.setText(abstractWeapon);
    }
}
