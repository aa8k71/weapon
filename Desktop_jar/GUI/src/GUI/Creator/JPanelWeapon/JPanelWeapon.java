package GUI.Creator.JPanelWeapon;

import Weapon.AbstractWeapon;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class JPanelWeapon extends JPanel {


    private JTextField jTextField_name = new JTextField();
    private JLabel jLabel_name = new JLabel("Name:");

    JPanelWeapon(){
        setjTextField(jTextField_name,90);
        setjLabel(jLabel_name,70);
        setVisible(true);
    }

    void setjTextField(JTextField jTextField,int y){
        jTextField.setBounds(20,y,300,20);
        this.add(jTextField);
    }

    void setjLabel(JLabel jLabel,int y){
        jLabel.setBounds(21,y,100,15);
        this.add(jLabel);
    }

    public HashMap<String,String> getText(){
        HashMap<String,String> strings = new HashMap<>();
        strings.put("name",jTextField_name.getText());
        return strings;
    }

    public void paint(ArrayList<String> nameLabel){
        if(nameLabel.contains("name"))
            jLabel_name.setForeground(Color.red);
    }

    public void setColor(){
        jLabel_name.setForeground(Color.BLACK);
    }

    public void setText(AbstractWeapon abstractWeapon){
        jTextField_name.setText(abstractWeapon.getName());
    }

}
