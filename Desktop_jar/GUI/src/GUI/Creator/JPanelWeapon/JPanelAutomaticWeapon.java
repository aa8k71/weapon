package GUI.Creator.JPanelWeapon;

import Weapon.AbstractWeapon;
import Weapon.AutomaticWeapon;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class JPanelAutomaticWeapon extends JPanelFireArm {

    private JTextField jTextField_rate = new JTextField();
    private JLabel jLabel_rate = new JLabel("Rate:");

    public JPanelAutomaticWeapon(){
        setjTextField(jTextField_rate,315);
        setjLabel(jLabel_rate,295);
    }

    @Override
    public HashMap<String,String> getText() {
        HashMap<String,String> strings = new HashMap<>(super.getText());
        strings.put("rate",jTextField_rate.getText());
        return strings;
    }

    @Override
    public void paint(ArrayList<String> nameLabel) {
        super.paint(nameLabel);
        if(nameLabel.contains("rate"))
            jLabel_rate.setForeground(Color.red);
    }

    @Override
    public void setColor() {
        super.setColor();
        jLabel_rate.setForeground(Color.BLACK);
    }

    @Override
    public void setText(AbstractWeapon abstractWeapon) {
        jTextField_rate.setText(String.valueOf((int)((AutomaticWeapon)abstractWeapon).getRate()));
        super.setText(abstractWeapon);
    }
}
