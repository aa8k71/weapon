package GUI.DivisionDesktop;

import GUI.Creator.AddMenu;
import Weapon.AbstractWeapon;
import Weapon.Division;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;

public class MainMenu extends JFrame {

    private JButton jButton_add;
    private JButton jButton_edit;
    private JButton jButton_delete;
    private JList jList_weapons;
    private DefaultListModel<String> defaultListModel_weapons;
    private ArrayList<AbstractWeapon> weapons;
    private JPanel jPanel = new JPanel();
    private Division division;
    private JLabel jLabel;

    public MainMenu(){}
    public MainMenu(int width, int height,Division division){
        this.division = division;
        setTitle("Division");
        setBounds(Toolkit.getDefaultToolkit().getScreenSize().width/2-width/2,
                Toolkit.getDefaultToolkit().getScreenSize().height/2 - height/2, width,height);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(jPanel);
        jPanel.setLayout(null);
        setjLabel();
        setjButton_add();
        setjButton_edit();
        setjButton_delete();
        setjList_weapons();
        setVisible(true);
        setResizable(false);
    }

    private void setjLabel(){
        jLabel = new JLabel();
        jLabel.setBounds(10,330,100,10);
        updatejLabel();
        jPanel.add(jLabel);
    }

    private void updatejLabel(){
        jLabel.setText("Cost:" + division.countCost() + " c.u");
    }

    private void setjButton_add(){
        jButton_add = new JButton("add");
        jPanel.add(jButton_add);
        jButton_add.setBounds(10,10,100,40);
        jButton_add.addActionListener((ActionEvent e) ->{
                setEnabled(false);
                new AddMenu(350,450, MainMenu.this);
            }
        );
    }

    private void setjButton_edit(){
        jButton_edit = new JButton("edit");
        jPanel.add(jButton_edit);
        jButton_edit.setBounds(10,60,100,40);
        jButton_edit.addActionListener((ActionEvent e) ->{
            int i = jList_weapons.getSelectedIndex();
            if(i == -1)
                return;
            setEnabled(false);
            new AddMenu(350,450,MainMenu.this,weapons.get(i));

        });
    }

    private void setjButton_delete(){
        jButton_delete = new JButton("delete");
        jPanel.add(jButton_delete);
        jButton_delete.setBounds(10,110,100,40);
        jButton_delete.addActionListener( (ActionEvent e) ->{
            int i = jList_weapons.getSelectedIndex();
            if(i == -1)
                return;
            division.removeWeapon(weapons.get(i));
            weapons.remove(i);
            defaultListModel_weapons.remove(i);
            updatejLabel();
        });
    }

    private void setjList_weapons(){
        setDefaultListModel_weapons();
        jList_weapons = new JList<>(defaultListModel_weapons);
        jList_weapons.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jList_weapons.setBounds(120, 10,205,330);
        jPanel.add(jList_weapons);
    }

    private void setDefaultListModel_weapons(){
        defaultListModel_weapons = new DefaultListModel<>();
        weapons = new ArrayList<>();
        for(HashMap.Entry<AbstractWeapon,Integer> i : division.getWeapons().entrySet()) {
            for(int j = 0; j < i.getValue(); j++) {
                defaultListModel_weapons.addElement(i.getKey().getName() + " " + i.getKey().getCost() + " c.u.");
                weapons.add(i.getKey());
            }
        }
    }

    public void addNewWeapon(AbstractWeapon abstractWeapon){
        division.addWeapon(abstractWeapon);
        defaultListModel_weapons.addElement(abstractWeapon.getName() + " " + abstractWeapon.getCost() + " c.u.");
        weapons.add(abstractWeapon);
        updatejLabel();
    }

    public void updateJList(){
        setDefaultListModel_weapons();
        jList_weapons.setModel(defaultListModel_weapons);
        updatejLabel();
    }

}
