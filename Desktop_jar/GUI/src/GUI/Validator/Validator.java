package GUI.Validator;

import Database.DataBase;
import Database.DataCSV;

import java.util.ArrayList;
import java.util.HashMap;

public class Validator {

    public static ArrayList<String> dataCheckNull(HashMap<String,String> stringHashMap){
        ArrayList<String> strings_null = new ArrayList<>();
        for(HashMap.Entry<String,String> i : stringHashMap.entrySet())
            if(i.getValue().equals(""))
                strings_null.add(i.getKey());
        return strings_null;
    }

    public static ArrayList<String> dataCheckInt(HashMap<String,String> stringHashMap){
        ArrayList<String> strings_int = new ArrayList<>();
        String[] numberFormat = {"distance","speed","capacity","rate","lenght"};
        for(HashMap.Entry<String,String> i : stringHashMap.entrySet())
            for(String k : numberFormat)
                if(k.equals(i.getKey()))
                    if(!i.getValue().matches("([1-9]\\d*)"))
                    strings_int.add(i.getKey());
        return strings_int;
    }

    public static ArrayList<String> dataCheckDatabase(HashMap<String,String> stringHashMap){
        ArrayList<String> strings_int = new ArrayList<>();
        String[] numberFormat = {"distance","speed","rate","lenght"};
        for(HashMap.Entry<String,String> i : stringHashMap.entrySet())
            for(String k : numberFormat)
                if(k.equals(i.getKey())) {
                    try{
                        if (new DataCSV().getMax(k) < Integer.valueOf(i.getValue()) ||
                                new DataCSV().getMin(k) > Integer.valueOf(i.getValue())) {
                            System.out.println(new DataCSV().getMax(k) + " " + new DataCSV().getMin(k) + i.getValue());
                            strings_int.add(i.getKey());
                        }
                    }
                    catch (NumberFormatException e){
                        strings_int.add(i.getKey());
                    }
                }
        return strings_int;
    }

}
