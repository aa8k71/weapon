package GUI.Validator;

import Database.DataBase;
import Database.DataCSV;
import GUI.Creator.JPanelWeapon.JPanelWeapon;

import javax.swing.*;
import java.util.ArrayList;

public class Handling {

    public static boolean handlingNull(JPanelWeapon jPanel){
        ArrayList<String> empty_values = Validator.dataCheckNull(jPanel.getText());
        if(empty_values.size() != 0) {
            JOptionPane.showMessageDialog(jPanel,"Заполните пустые поля");
            jPanel.paint(empty_values);
            return false;
        }
        return true;
}

    public static boolean handlingWrongNumber(JPanelWeapon jPanel){
        ArrayList<String> wrong_number_values = Validator.dataCheckInt(jPanel.getText());
        if(wrong_number_values.size() != 0){
            String error = "";
            for(String i : wrong_number_values)
                error = error.concat(i + ", ");
            if(wrong_number_values.size() == 1)
                JOptionPane.showMessageDialog(jPanel,"Полe " + error.substring(0,error.length()-2) +
                        " должно содержать целочисленное значение");
            else JOptionPane.showMessageDialog(jPanel,"Поля " + error.substring(0,error.length()-2) +
                    " должны содержать целочисленные значения");
            jPanel.paint(wrong_number_values);;
            return false;
        }
        return true;
    }

    public static boolean handlingWrongValue(JPanelWeapon jPanel){
        ArrayList<String> wrong_database_values = Validator.dataCheckDatabase(jPanel.getText());
        if(wrong_database_values.size() != 0){
            jPanel.paint(wrong_database_values);
            String error = "";
            for(String i : wrong_database_values)
                error = error.concat(i + ": (" +  new DataCSV().getMin(i)+ " - " + new DataCSV().getMax(i) + ")\n");
            if(wrong_database_values.size() == 1)
                JOptionPane.showMessageDialog(jPanel, "Некорректное значение\n" + "Допустимое значение: " + error);
            else JOptionPane.showMessageDialog(jPanel, "Некорректные значения\n" + "Допустимые значения: " + error);
            return false;
        }
        return true;
    }
}
